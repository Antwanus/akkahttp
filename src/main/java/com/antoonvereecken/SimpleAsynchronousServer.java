package com.antoonvereecken;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.AskPattern;
import akka.actor.typed.javadsl.Behaviors;
import akka.http.javadsl.Http;
import akka.http.javadsl.IncomingConnection;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.*;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.japi.function.Function;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class SimpleAsynchronousServer {
    ActorSystem<CustomerBehavior.Command> actorSystem = ActorSystem.create(CustomerBehavior.create(), "actorSysAsyncServer");

    Function<HttpRequest, CompletionStage<HttpResponse>> asyncMethodHandler = (HttpRequest httpRequest) -> {
        CompletableFuture<HttpResponse> httpResponseCF = new CompletableFuture<>();
        /** :8080/api/customer */
        if (httpRequest.getUri().path().equalsIgnoreCase("/api/customer")) {
            if (httpRequest.method() == HttpMethods.POST) {
                // add a customer
                Unmarshaller<HttpEntity, Customer> jsonToCustomerMapper = Jackson.unmarshaller(Customer.class);
                CompletionStage<Customer> customerFuture = jsonToCustomerMapper.unmarshal(httpRequest.entity(), actorSystem);
                //todo customer != null ?
                customerFuture.whenComplete((customer, throwable) -> {
                   //todo throwable != null ?
                   actorSystem.tell(new CustomerBehavior.UpdateCustomerCommand(customer));
                   httpRequest.discardEntityBytes(actorSystem); // clears data (not rly needed here, but good practice)
                   httpResponseCF.complete(HttpResponse.create().withStatus(200));
                });
            }
            else if (httpRequest.method() == HttpMethods.GET) {
                // get all customers
                CompletionStage<List<Customer>> customerListFuture = AskPattern.ask(
                        actorSystem,
                        replyTo -> new CustomerBehavior.GetCustomersCommand(replyTo),
                        Duration.ofSeconds(5),
                        actorSystem.scheduler()
                );
                customerListFuture.whenComplete((customers, throwable) -> {
                    //todo throwable != null ?
                    try {
                        String customerListJson = new ObjectMapper().writeValueAsString(customers);
                        httpResponseCF.complete(HttpResponse.create().withStatus(200).withEntity(customerListJson));
                        httpRequest.discardEntityBytes(actorSystem);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                        httpResponseCF.complete(HttpResponse.create().withStatus(StatusCodes.INTERNAL_SERVER_ERROR));
                        httpRequest.discardEntityBytes(actorSystem);
                    }

                });
            }
            else {
                httpResponseCF.complete(HttpResponse.create().withStatus(StatusCodes.METHOD_NOT_ALLOWED));
                httpRequest.discardEntityBytes(actorSystem);
            }
            //process
        }
        /***/
        else {
            httpResponseCF.complete(HttpResponse.create().withStatus(StatusCodes.NOT_FOUND));
            httpRequest.discardEntityBytes(actorSystem);
        }
        return httpResponseCF;
    };

    void run() {
        Source<IncomingConnection, CompletionStage<ServerBinding>> incConnectionSource = Http.get(actorSystem)
                .newServerAt("localhost", 8080)
                .connectionSource();

        Flow<IncomingConnection, IncomingConnection, NotUsed> flow = Flow.of(IncomingConnection.class).map(
                connection -> {
                    System.out.println("connection.remoteAddress() ==>" + connection.remoteAddress());
                    connection.handleWithAsyncHandler(asyncMethodHandler, Materializer.createMaterializer(actorSystem));

                    return connection;
                });

        Sink<IncomingConnection, CompletionStage<Done>> sink = Sink.ignore();

        CompletionStage<ServerBinding> server = incConnectionSource.via(flow).to(sink).run(actorSystem);

        server.whenComplete((serverBinding, throwable) -> {
            if (throwable != null) System.out.println("something went wrong " +throwable);
            else System.out.println("serverBinding.localAddress() ==> " +serverBinding.localAddress());

        });

    }

}
