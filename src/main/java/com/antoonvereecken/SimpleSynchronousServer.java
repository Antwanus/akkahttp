package com.antoonvereecken;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.http.javadsl.Http;
import akka.http.javadsl.IncomingConnection;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.RequestEntity;
import akka.http.javadsl.model.StatusCodes;
import akka.japi.function.Function;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

import java.util.concurrent.CompletionStage;

public class SimpleSynchronousServer {
    ActorSystem<?> actorSystem = ActorSystem.create(Behaviors.empty(), "simpleSyncServerActorSystem");

    Function<HttpRequest, HttpResponse> syncMethodHandler = (httpRequest) -> {
        RequestEntity requestEntity = httpRequest.entity();

        System.out.println("----------------------------------------*** httpRequest.getHeaders() ***----------------------------------------");
        httpRequest.getHeaders().forEach(header ->
                System.out.println(header.name() + " : " + header.value())
        );
        System.out.println("-----------------------------------------------------------------------------------------------------------------");
        System.out.println("httpRequest.getUri()   ==   " + httpRequest.getUri().getPathString());
        if (httpRequest.getUri().rawQueryString().isPresent()) {
            System.out.println("httpRequest.getUri().rawQueryString().get()  ==   "
                    + httpRequest.getUri().rawQueryString().get());
        }
        //simply put, consumes/completes connection (=discard data)
        httpRequest.discardEntityBytes(actorSystem);

        return HttpResponse.create()
                .withStatus(StatusCodes.OK).withEntity("data was received");
    };

    void run() {
        Source<IncomingConnection, CompletionStage<ServerBinding>> incConnectionSource = Http.get(actorSystem)
                .newServerAt("localhost", 8080)
                .connectionSource();

        Flow<IncomingConnection, IncomingConnection, NotUsed> flow = Flow.of(IncomingConnection.class).map(
            connection -> {
                System.out.println("connection.remoteAddress() ==>" + connection.remoteAddress());
                connection.handleWithSyncHandler(syncMethodHandler, Materializer.createMaterializer(actorSystem));

                return connection;
            });

        Sink<IncomingConnection, CompletionStage<Done>> sink = Sink.ignore();

        CompletionStage<ServerBinding> server = incConnectionSource.via(flow).to(sink).run(actorSystem);

        server.whenComplete((serverBinding, throwable) -> {
            if (throwable != null) System.out.println("something went wrong " +throwable);
            else System.out.println("serverBinding.localAddress() ==> " +serverBinding.localAddress());

        });

    }

}
