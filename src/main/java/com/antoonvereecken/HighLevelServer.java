package com.antoonvereecken;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.AskPattern;
import akka.http.javadsl.*;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.*;
import akka.http.javadsl.server.*;
import akka.http.javadsl.server.directives.SecurityDirectives;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.japi.Option;
import akka.japi.function.Function;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.antoonvereecken.security.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Duration;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Supplier;

import static akka.http.javadsl.server.Directives.*;
import static akka.http.javadsl.server.PathMatchers.*;

public class HighLevelServer {
    ActorSystem<CustomerBehavior.Command> actorSystem = ActorSystem.create(CustomerBehavior.create(), "actorSysAsyncServer");

    Function<HttpRequest, CompletionStage<HttpResponse>> updateCustomer = request -> {
            CompletableFuture<HttpResponse> response = new CompletableFuture<>();
            Unmarshaller<HttpEntity, Customer> jsonToCustomerMapper = Jackson.unmarshaller(Customer.class);
            CompletionStage<Customer> customerFuture = jsonToCustomerMapper.unmarshal(request.entity(), actorSystem);
            customerFuture.whenComplete((customer, throwable) -> {
                if (throwable != null) {
                    response.complete(  HttpResponse.create()
                                                    .withStatus(StatusCodes.BAD_REQUEST)
                                                    .withEntity(throwable.toString()));
                } else {
                    actorSystem.tell(new CustomerBehavior.UpdateCustomerCommand(customer));
                    request.discardEntityBytes(actorSystem); // clears data (not rly needed here, but good practice)
                    response.complete(HttpResponse.create().withStatus(200));
                }
            });
            return response;
    };
    Function<HttpRequest, CompletionStage<HttpResponse>> getAllCustomers = request -> {
        System.out.println("--------HEADERS-----------");
        request.getHeaders().forEach(e -> System.out.println(e.name() +" : "+ e.value()));
        System.out.println("------END_HEADERS---------");
        CompletableFuture<HttpResponse> response = new CompletableFuture<>();
        CompletionStage<List<Customer>> customerListFuture = AskPattern.ask(
                actorSystem,
                replyTo -> new CustomerBehavior.GetCustomersCommand(replyTo),
                Duration.ofSeconds(5),
                actorSystem.scheduler()
        );
        customerListFuture.whenComplete((customers, throwable) -> {
            //todo throwable != null ?
            try {
                String customerListJson = new ObjectMapper().writeValueAsString(customers);
                response.complete(HttpResponse.create().withStatus(200).withEntity(customerListJson));
                request.discardEntityBytes(actorSystem);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                response.complete(HttpResponse.create().withStatus(StatusCodes.INTERNAL_SERVER_ERROR));
                request.discardEntityBytes(actorSystem);
            }
        });
        return response;
    };
    CompletionStage<HttpResponse> getCustomerById(int id, HttpRequest request) {
        CompletableFuture<HttpResponse> response = new CompletableFuture<>();
        CompletionStage<Customer> customerFuture = AskPattern.ask(actorSystem,
                replyTo -> new CustomerBehavior.GetCustomerCommand(id, replyTo),
                Duration.ofSeconds(5),
                actorSystem.scheduler()
        );
        customerFuture.whenComplete((customer, throwable) -> {
           if (throwable == null) {
               try {
                   if (customer.getId() == 0)
                       response.complete(HttpResponse.create().withStatus(StatusCodes.NO_CONTENT));
                   else {
                       String json = new ObjectMapper().writeValueAsString(customer);
                       response.complete(HttpResponse.create().withStatus(200).withEntity(json));
                   }
               } catch (JsonProcessingException e) {
                   //should never happen
                   e.printStackTrace();
                   response.complete(HttpResponse.create().withStatus(StatusCodes.INTERNAL_SERVER_ERROR));
               }
           } else response.complete(HttpResponse.create().withStatus(StatusCodes.NO_CONTENT));

        });
        return response;
    }

    Route authenticatedPost(Supplier<Route> inner) {
        return authenticateBasic("secure", basicAuthenticator, user ->
            authorize(user::isAdmin, ()->
                    post(inner::get)
        ));
    }

    private Route createRoute() {
        PathMatcher0 entryPoint = separateOnSlashes("api/v3").orElse(separateOnSlashes("api/v2"));
        PathMatcher1<Integer> customerUrlWithId = segment("customer").slash(integerSegment());

        Route customerRoute = path("customer", ()->
                concat(
                        get(    ()-> parameter("id", id ->
                                extractRequest(request -> {
                                    try {
                                        return completeWithFuture(getCustomerById(Integer.parseInt(id), request));
                                    } catch (Exception e) {
                                        return reject(Rejections.invalidRequiredValueForQueryParam("id", "integer", "not an integer"));
                                    }
                                })
                        )),
                        get(    ()-> authenticateBasicEncrypted( user-> handle(getAllCustomers) )),
                        authenticatedPost( ()-> handle(updateCustomer))
        ));

        return concat(
            pathPrefix(entryPoint , ()->
                concat(
                    path(customerUrlWithId, (id ->
                            extractRequest(request ->
                                    completeWithFuture( getCustomerById(id, request)))
                    )),
                    customerRoute,
                    path("alive", ()->
                            get(    ()-> complete("I 'm still alive"))
                    )
                )
            ),
            pathEndOrSingleSlash(Directives::reject)
        );
    }

    void run() {
        CompletionStage<ServerBinding> server = Http.get(actorSystem)
                                                    .newServerAt("localhost", 443)  //normal to use 443 for SSL
                                                    .enableHttps(createHttpsContext())
                                                    .bind(createRoute());

        server.whenComplete((serverBinding, throwable) -> {
            if (throwable != null) System.out.println("something went wrong " +throwable);
            else System.out.println("serverBinding.localAddress() ==> " +serverBinding.localAddress());

        });
    }

    private HttpsConnectionContext createHttpsContext() {
        String password = getCertificatePasswordFromPropsFile();
        try (
            InputStream inputStream = ClassLoader.getSystemResourceAsStream("identity.p12");
        ){
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(inputStream, password.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keyStore, password.toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(keyStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

            return ConnectionContext.httpsServer(sslContext);
        } catch (   KeyStoreException | IOException | CertificateException  | NoSuchAlgorithmException |
                    UnrecoverableKeyException | KeyManagementException e
            )   {
                    e.printStackTrace();
                    throw new RuntimeException(e);
        }
    }
    /** reads property in .../resources/security.properties */
    private String getCertificatePasswordFromPropsFile() {
        try (
                InputStream inputStream = ClassLoader.getSystemResourceAsStream("security.properties");
            ) {
                Properties p = new Properties();
                p.load(inputStream);
                return p.getProperty("cerficiatepassword");
        }
        catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /** we can use Akka's ProvidedCredentials (base64) api -> as long as it is not encrypted */
    private final java.util.function.Function<    Optional<SecurityDirectives.ProvidedCredentials>,
                                            Optional<User>  > basicAuthenticator =
            providedCredentials -> {
                final List<User> users = List.of(
                    new User("user", "pw", false),
                    new User("admin", "pw", true)
                );
                if ( providedCredentials.isPresent() ) {
                    String username = providedCredentials.get().identifier();
                    Optional<User> foundUser = users.stream()
                                                    .filter(e -> e.getUsername().equals(username))
                                                    .findFirst();
                    if ( foundUser.isPresent() ) {  // then let akka compare the credentials
                        if ( providedCredentials.get().verify(foundUser.get().getPassword()) ){
                            return foundUser;   // return foundUser if passwords match
                }}}
                return Optional.empty();
    };
    private Optional<User> authenticateUserEncrypted(String token) {
        final List<User> users = List.of(
                new User("user", "pw", false),
                new User("admin", "pw", true)
        );
        System.out.println("***     decoding token with Base64...");
        String usernameAndPassword = new String(Base64.getDecoder().decode(token));
        System.out.println("username:password   =   " + usernameAndPassword);
        String username = usernameAndPassword.split(":")[0];
        String pw = usernameAndPassword.split(":")[1];

        return users.stream()
                .filter(e -> e.getUsername().equals(username) && e.getPassword().equals(pw))
                .findFirst();
    }

    private Route authenticateBasicEncrypted(java.util.function.Function<User, Route> inner) {
        return headerValueByName("Authorization", basicAuthHeader -> {
            System.out.println("basicAuthHeader = " +basicAuthHeader);
            if (basicAuthHeader.startsWith("Basic")) {
                String token =  basicAuthHeader.substring(6);
                Optional<User> foundUser = authenticateUserEncrypted(token);
                if (foundUser.isPresent()) {
                    return inner.apply(foundUser.get());
                } else return complete(StatusCodes.FORBIDDEN);
            } else return complete(StatusCodes.FORBIDDEN);
        });
    }


}
