package com.antoonvereecken;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonMarshallizor {
    public static void main(String[] args) {
        Customer customer = new Customer(1, "first", "last", 1, true);
        String jsonString = "";
        try {
            jsonString = new ObjectMapper().writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println("marshalled javaObject to jsonString   ==>   "+jsonString);

        Customer mappedCustomer = null;
        try {
            mappedCustomer = new ObjectMapper().readValue(jsonString, Customer.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println("unmarshalled jsonString to javaObject   ==>   " +mappedCustomer);


    }
}
